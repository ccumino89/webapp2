# webapp2: Basic webapp with vuejs 3 + bootstrap 5

## Demo

![webapp demo](public/demo.gif "Web app demo")

## Features

- skeleton created with vue-cli 4.5 using Vue 3 / babel / eslint presets

- SPA (single-page-application) with dynamic routing, see [/src/router/index.js](/src/router/index.js)

- reusable components, see [components dir](/src/components)

- [bootstrap5 can be tweaked using SCSS](https://getbootstrap.com/docs/5.0/customize/sass/#modify-map), see `<style></style>` section in `src/App.vue`.

## Local install

### Prerequisites

Debian 11 (bullseye):
```
sudo apt install yarnpkg
```

MacOS:

```
brew update
brew install yarn
```

Windows:

Install [Node.js](https://nodejs.org/en/download/)
Be sure to add Windows PATH

Then install node dependencies with:

```
yarnpkg install
```

On Windows install node modules:

```
npm install 
```

### Compiles and hot-reloads for development
```
yarnpkg serve
```

Initialize server on Windows:
```
npm run serve 
```

Open the webapp at http://localhost:8080

### Compiles and minifies for production
```
yarnpkg build
```

The static files to install on the production webserver will be under `/dist`.

### Lints and fixes files
```
yarnpkg lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
